from flask import Flask, jsonify, request
import message_controller
import serial
import serial.tools.list_ports as list_ports
from threading import Thread
import requests

PID_MICROBIT = 516
VID_MICROBIT = 3368
TIMEOUT = 10
PORT = 7000
PORT_RESIDENCIA = 5000

def find_comport(pid,vid,baud):
	ser_port = serial.Serial(timeout=TIMEOUT)
	ser_port.baudrate = baud
	ports = list(list_ports.comports())
	print('scanning ports')
	for p in ports:
		print('port: {}'.format(p))
		try:
			print('pid: {} vid: {}'.format(p.pid,p.vid))
		except AttributeError:
			continue
		if (p.pid == pid) and (p.vid == vid):
			print('found target device pid: {} vid: {} port: {}'.format(
				p.pid, p.vid, p.device))
			ser_port.port = str(p.device)
			return ser_port
	return None
        
app = Flask(__name__)

@app.route("/mensaje", methods=["POST"])
def insert_mensaje():
	print('insertando mensaje')
	mensaje_details = request.get_json()
	contenido = mensaje_details["name"]
	contenidoBytes = contenido.encode('utf-8')
	ser_micro.write(contenidoBytes)
	return "Enviado"
	
def receive(period, message):
	ser_micro = message
	while 1:
		leido = ser_micro.readline()
		line = leido.decode('utf-8')
		if line:
			mensaje_microbit = '{"name": "' + line[:-2] + '"}'
			URL = "http://localhost:" + str(PORT_RESIDENCIA) + "/mensaje"
			headers = {
				"Content-Type": "application/json"
			}
			print("Message: " + mensaje_microbit)
			respuesta = requests.post(URL, headers = headers, data = mensaje_microbit)
			print(respuesta.content)
			

if __name__ == "__main__":
	global ser_micro
	print('looking for microbit')
	ser_micro = find_comport(PID_MICROBIT, VID_MICROBIT, 9600)
	if not ser_micro:
		print('microbit not found')
	print('opening and monitoring microbit port')
	ser_micro.open()
	
	t = Thread(target=receive, args=[1, ser_micro])
	t.start()
	app.run(host='0.0.0.0', port=PORT, debug=False)
	ser_micro.close()
