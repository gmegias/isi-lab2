import sqlite3
from flask import Flask, jsonify, request
DATABASE_NAME = "residencia.db"
PORT = 5000

def get_db():
	conn = sqlite3.connect(DATABASE_NAME)
	return conn
    
def create_table():
	tables = [
		"""CREATE TABLE IF NOT EXISTS residencia(
				id INTEGER PRIMARY KEY AUTOINCREMENT,
				name TEXT NOT NULL
			)
		"""]
	db = get_db()
	cursor = db.cursor()
	for table in tables:
		cursor.execute(table)

app = Flask(__name__)

@app.route("/mensaje", methods=["POST"])
def insert_mensaje():
	contenido = request.get_json()
	mensaje = contenido["name"]
	db = get_db()
	cursor = db.cursor()
	statement = "INSERT INTO residencia(name) VALUES (?)"
	cursor.execute(statement, [mensaje])
	db.commit()
	return "Guardado en DB"

@app.route("/mensaje", methods=["GET"])
def get_mensaje():
	db = get_db()
	cursor = db.cursor()
	query = "SELECT id, name FROM residencia"
	cursor.execute(query)
	return cursor.fetchall()

if __name__ == "__main__":
	create_table()
	app.run(host='0.0.0.0', port=PORT, debug=False)
